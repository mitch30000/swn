# SWN

A set of utility resources made for use with Stars Without Number (SWN), a popular sci-fi tabletop RPG.  These utilities were made to be run using Python 3 via command line in order to get quick results.  The currently supported features of this utility are:
 - `python swn.py --help`: Shows the help menu explaining usage of the utility and all supported features.
 - `python swn.py --create-npc`: Creates a random NPC based on the rules given in the revised SWN handbook.
 - `python swn.py --create-patron`: Creates a random Patron based on the rules given in the revised SWN handbook.