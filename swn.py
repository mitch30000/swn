import argparse
import npc
import patron

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Stars Without Number RPG utilities')
    parser.add_argument('-npc', '--create-npc', action='store_true', help='Create a random NPC')
    parser.add_argument('-patron', '--create-patron', action='store_true', help='Create a random Patron')
    args = parser.parse_args()

    if args.create_npc:
        print(npc.get_result())
    elif args.create_patron:
        print(patron.get_result())
