import names
import random


def get_random_number(max_number):
    return random.randint(1, max_number)


def get_gender():
    gender_roll = get_random_number(2)
    return 'Male' if (gender_roll == 1) else 'Female'


def get_age():
    age_roll = get_random_number(4)
    if age_roll == 1:
        return 'Unusually young or old for their role'
    elif age_roll == 2:
        return 'Young adult'
    elif age_roll == 3:
        return 'Mature prime'
    else:
        return 'Middle-aged or elderly'


def get_background():
    background_roll = get_random_number(6)
    if background_roll == 1:
        return 'The local underclass or poorest natives'
    elif background_roll == 2:
        return 'Common laborers or cube workers'
    elif background_roll == 3:
        return 'Aspiring bourgeoise or upper class'
    elif background_roll == 4:
        return 'The elite of this society'
    elif background_roll == 5:
        # Minority or foreigners; reroll on 1d4
        reroll = get_random_number(4)
        if reroll == 1 or reroll == 2:
            return 'Minority'
        else:
            return 'Foreigner'
    else:
        # Offworlders or exotics; reroll on 1d4
        reroll = get_random_number(4)
        if reroll == 1 or reroll == 2:
            return 'Offworlders'
        else:
            return 'Exotics'


def get_role():
    role_roll = get_random_number(8)
    if role_roll == 1:
        return 'Criminal, thug, thief, swindler'
    elif role_roll == 2:
        return 'Menial, cleaner, retail worker, servant'
    elif role_roll == 3:
        return 'Unskilled heavy labor, porter, construction'
    elif role_roll == 4:
        return 'Skilled trade, electrician, mechanic, pilot'
    elif role_roll == 5:
        return 'Idea worker, programmer, writer'
    elif role_roll == 6:
        return 'Merchant, business owner, trader, banker'
    elif role_roll == 7:
        return 'Official, bureaucrat, courtier, clerk'
    else:
        return 'Military, soldier, enforcer, law officer'


def get_problem():
    problem_roll = get_random_number(10)
    if problem_roll == 1:
        return 'They have significant debt or money woes'
    elif problem_roll == 2:
        return 'A loved one is in trouble'
    elif problem_roll == 3:
        return 'Romantic failure with a desired person'
    elif problem_roll == 4:
        return 'Drug or behavioral addiction'
    elif problem_roll == 5:
        return 'Their superior dislikes or resents them'
    elif problem_roll == 6:
        return 'They have a persistent sickness'
    elif problem_roll == 7:
        return 'They hate their job or life situation'
    elif problem_roll == 8:
        return 'Someone dangerous is targeting them'
    elif problem_roll == 9:
        return 'They’re pursuing a disastrous purpose'
    else:
        return 'They have no problems worth mentioning'


def get_desire():
    desire_roll = get_random_number(12)
    if desire_roll == 1:
        return 'They want a particular romantic partner'
    elif desire_roll == 2:
        return 'They want money for them or a loved one'
    elif desire_roll == 3:
        return 'They want a promotion in their job'
    elif desire_roll == 4:
        return 'They want answers about a past trauma'
    elif desire_roll == 5:
        return 'They want revenge on an enemy'
    elif desire_roll == 6:
        return 'They want to help a beleaguered friend'
    elif desire_roll == 7:
        return 'They want an entirely different job'
    elif desire_roll == 8:
        return 'They want protection from an enemy'
    elif desire_roll == 9:
        return 'They want to leave their current life'
    elif desire_roll == 10:
        return 'They want fame and glory'
    elif desire_roll == 11:
        return 'They want power over those around them'
    else:
        return 'They have everything they want from life'


def get_trait():
    trait_roll = get_random_number(20)
    if trait_roll == 1:
        return 'Ambition'
    elif trait_roll == 2:
        return 'Avarice'
    elif trait_roll == 3:
        return 'Bitterness'
    elif trait_roll == 4:
        return 'Courage'
    elif trait_roll == 5:
        return 'Cowardice'
    elif trait_roll == 6:
        return 'Curiosity'
    elif trait_roll == 7:
        return 'Deceitfulness'
    elif trait_roll == 8:
        return 'Determination'
    elif trait_roll == 9:
        return 'Devotion to a cause'
    elif trait_roll == 10:
        return 'Filiality'
    elif trait_roll == 11:
        return 'Hatred'
    elif trait_roll == 12:
        return 'Honesty'
    elif trait_roll == 13:
        return 'Hopefulness'
    elif trait_roll == 14:
        return 'Love of a person'
    elif trait_roll == 15:
        return 'Nihilism'
    elif trait_roll == 16:
        return 'Paternalism'
    elif trait_roll == 17:
        return 'Pessimism'
    elif trait_roll == 18:
        return 'Protectiveness'
    elif trait_roll == 19:
        return 'Resentment'
    else:
        return 'Shame'


def get_result():
    gender = get_gender()
    name = names.get_english_name(gender)
    background = get_background()
    role = get_role()
    problem = get_problem()
    desire = get_desire()
    trait = get_trait()
    result = 'Gender=\'' + gender + '\'\n'
    result += 'Name=\'' + name + '\'\n'
    result += 'Background=\'' + background + '\'\n'
    result += 'Role=\'' + role + '\'\n'
    result += 'Problem=\'' + problem + '\'\n'
    result += 'Desire=\'' + desire + '\'\n'
    result += 'Trait=\'' + trait + '\'\n'
    return result
