import names
import random


def get_random_number(max_number):
    return random.randint(1, max_number)


def get_gender():
    gender_roll = get_random_number(2)
    return 'Male' if (gender_roll == 1) else 'Female'


def get_eagerness_to_hire():
    hire_roll = get_random_number(4)
    if hire_roll == 1:
        return 'Cautious, but can be convinced to hire'
    elif hire_roll == 2:
        return 'Willing to promise standard rates'
    elif hire_roll == 3:
        return 'Eager, willing to offer a bonus'
    else:
        return 'Desperate, might offer what they can’t pay'


def get_trustworthiness():
    trust_roll = get_random_number(6)
    if trust_roll == 1:
        return 'They intend to totally screw the PCs'
    elif trust_roll == 2:
        return 'They won’t pay unless forced to do so'
    elif trust_roll == 3:
        return 'They’ll pay slowly or reluctantly'
    elif trust_roll == 4:
        return 'They’ll pay, but discount for mistakes'
    elif trust_roll == 5:
        return 'They’ll pay without quibbling'
    else:
        return 'They’ll pay more than they promised'


def get_job_info():
    job_roll = get_random_number(8)
    if job_roll == 1:
        return 'Kill somebody who might deserve it'
    elif job_roll == 2:
        return 'Kidnap someone dangerous'
    elif job_roll == 3:
        return 'Steal a well-guarded object'
    elif job_roll == 4:
        return 'Arson or sabotage on a place'
    elif job_roll == 5:
        return 'Get proof of some misdeed'
    elif job_roll == 6:
        return 'Protect someone from an immediate threat'
    elif job_roll == 7:
        return 'Transport someone through danger'
    else:
        return 'Guard an object being transported'


def get_counter_force():
    force_roll = get_random_number(10)
    if force_roll == 1:
        return 'A treacherous employer or subordinate'
    elif force_roll == 2:
        return 'An open and known enemy of the patron'
    elif force_roll == 3:
        return 'Official governmental meddling'
    elif force_roll == 4:
        return 'An unknown rival of the patron'
    elif force_roll == 5:
        return 'The macguffin itself opposes them'
    elif force_roll == 6:
        return 'Very short time frame allowed'
    elif force_roll == 7:
        return 'The job is spectacularly illegal'
    elif force_roll == 8:
        return 'A participant would profit by their failure'
    elif force_roll == 9:
        return 'The patron is badly wrong about a thing'
    else:
        return 'The locals are against the patron'


def get_reward():
    reward_roll = get_random_number(12)
    if reward_roll == 1:
        return 'Government official favors owed'
    elif reward_roll == 2:
        return 'Property in the area'
    elif reward_roll == 3:
        return 'An item very valuable on another world'
    elif reward_roll == 4:
        return 'Pretech mod components'
    elif reward_roll == 5:
        return 'Useful pretech artifact'
    elif reward_roll == 6:
        return 'Information the PCs need'
    elif reward_roll == 7:
        return 'Membership in a powerful group'
    elif reward_roll == 8:
        return 'Black market access'
    elif reward_roll == 9:
        return 'Use of restricted facilities or shipyards'
    elif reward_roll == 10:
        return 'Shares in a profitable business'
    elif reward_roll == 11:
        return 'Maps to a hidden or guarded treasure'
    else:
        return 'Illegal but valuable weapons or gear'


def get_job_complication():
    complication_roll = get_random_number(20)
    if complication_roll == 1:
        return 'An ambush is laid somewhere'
    elif complication_roll == 2:
        return 'PC involvement is leaked to the enemy'
    elif complication_roll == 3:
        return 'The patron gives faulty aid somehow'
    elif complication_roll == 4:
        return 'Failing would be extremely unhealthy'
    elif complication_roll == 5:
        return 'The job IDs them as allies of a local faction'
    elif complication_roll == 6:
        return 'The macguffin is physically dangerous'
    elif complication_roll == 7:
        return 'An important location is hard to get into'
    elif complication_roll == 8:
        return 'Succeeding would be morally distasteful'
    elif complication_roll == 9:
        return 'A supposed ally is very unhelpful or stupid'
    elif complication_roll == 10:
        return 'The patron badly misunderstood the PCs'
    elif complication_roll == 11:
        return 'The job changes suddenly partway through'
    elif complication_roll == 12:
        return 'An unexpected troublemaker is involved'
    elif complication_roll == 13:
        return 'Critical gear will fail partway through'
    elif complication_roll == 14:
        return 'An unrelated accident complicates things'
    elif complication_roll == 15:
        return 'Payment comes in a hard-to-handle form'
    elif complication_roll == 16:
        return 'Someone is turning traitor on the patron'
    elif complication_roll == 17:
        return 'A critical element has suddenly moved'
    elif complication_roll == 18:
        return 'Payment is in avidly-pursued hot goods'
    elif complication_roll == 19:
        return 'The true goal is a subsidiary part of the job'
    else:
        return 'No complications; it’s just as it seems to be'


def get_result():
    gender = get_gender()
    name = names.get_english_name(gender)
    trustworthiness = get_trustworthiness()
    job_info = get_job_info()
    counter_force = get_counter_force()
    reward = get_reward()
    job_complication = get_job_complication()
    result = 'Gender=\'' + gender + '\'\n'
    result += 'Name=\'' + name + '\'\n'
    result += 'Patron Trustworthiness=\'' + trustworthiness + '\'\n'
    result += 'Basic Challenge of the Job=\'' + job_info + '\'\n'
    result += 'Main Countervailing Force=\'' + counter_force + '\'\n'
    result += 'Potential Non-Cash Rewards=\'' + reward + '\'\n'
    result += 'Complication to the Job=\'' + job_complication + '\'\n'
    return result
