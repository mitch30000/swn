import random


def get_random_number(max_number):
    return random.randint(1, max_number)


def get_english_name(gender):
    male_given_name_dict = dict.fromkeys([1, 2, 3, 4], 'Adam')
    male_given_name_dict.update(dict.fromkeys([5, 6, 7, 8], 'Albert'))
    male_given_name_dict.update(dict.fromkeys([9, 10, 11, 12], 'Alfred'))
    male_given_name_dict.update(dict.fromkeys([13, 14, 15, 16], 'Allan'))
    male_given_name_dict.update(dict.fromkeys([17, 18, 19, 20], 'Archibald'))
    male_given_name_dict.update(dict.fromkeys([21, 22, 23, 24], 'Authur'))
    male_given_name_dict.update(dict.fromkeys([25, 26, 27, 28], 'Basil'))
    male_given_name_dict.update(dict.fromkeys([29, 30, 31, 32], 'Charles'))
    male_given_name_dict.update(dict.fromkeys([33, 34, 35, 36], 'Colin'))
    male_given_name_dict.update(dict.fromkeys([37, 38, 39, 40], 'Donald'))
    male_given_name_dict.update(dict.fromkeys([41, 42, 43, 44], 'Douglas'))
    male_given_name_dict.update(dict.fromkeys([45, 46, 47, 48], 'Edgar'))
    male_given_name_dict.update(dict.fromkeys([49, 50, 51, 52], 'Edmund'))
    male_given_name_dict.update(dict.fromkeys([53, 54, 55, 56], 'Edward'))
    male_given_name_dict.update(dict.fromkeys([57, 58, 59, 60], 'George'))
    male_given_name_dict.update(dict.fromkeys([61, 62, 63, 64], 'Harold'))
    male_given_name_dict.update(dict.fromkeys([65, 66, 67, 68], 'Henry'))
    male_given_name_dict.update(dict.fromkeys([69, 70, 71, 72], 'Ian'))
    male_given_name_dict.update(dict.fromkeys([73, 74, 75, 76], 'James'))
    male_given_name_dict.update(dict.fromkeys([77, 78, 79, 80], 'John'))
    male_given_name_dict.update(dict.fromkeys([81, 82, 83, 84], 'Lewis'))
    male_given_name_dict.update(dict.fromkeys([85, 86, 87, 88], 'Oliver'))
    male_given_name_dict.update(dict.fromkeys([89, 90, 91, 92], 'Philip'))
    male_given_name_dict.update(dict.fromkeys([93, 94, 95, 96], 'Richard'))
    male_given_name_dict.update(dict.fromkeys([97, 98, 99, 100], 'William'))

    female_given_name_dict = dict.fromkeys([1, 2, 3, 4], 'Abigail')
    female_given_name_dict.update(dict.fromkeys([5, 6, 7, 8], 'Anne'))
    female_given_name_dict.update(dict.fromkeys([9, 10, 11, 12], 'Beatrice'))
    female_given_name_dict.update(dict.fromkeys([13, 14, 15, 16], 'Blanche'))
    female_given_name_dict.update(dict.fromkeys([17, 18, 19, 20], 'Catherine'))
    female_given_name_dict.update(dict.fromkeys([21, 22, 23, 24], 'Charlotte'))
    female_given_name_dict.update(dict.fromkeys([25, 26, 27, 28], 'Claire'))
    female_given_name_dict.update(dict.fromkeys([29, 30, 31, 32], 'Eleanor'))
    female_given_name_dict.update(dict.fromkeys([33, 34, 35, 36], 'Elizabeth'))
    female_given_name_dict.update(dict.fromkeys([37, 38, 39, 40], 'Emily'))
    female_given_name_dict.update(dict.fromkeys([41, 42, 43, 44], 'Emma'))
    female_given_name_dict.update(dict.fromkeys([45, 46, 47, 48], 'Georgia'))
    female_given_name_dict.update(dict.fromkeys([49, 50, 51, 52], 'Harriet'))
    female_given_name_dict.update(dict.fromkeys([53, 54, 55, 56], 'Joan'))
    female_given_name_dict.update(dict.fromkeys([57, 58, 59, 60], 'Judy'))
    female_given_name_dict.update(dict.fromkeys([61, 62, 63, 64], 'Julia'))
    female_given_name_dict.update(dict.fromkeys([65, 66, 67, 68], 'Lucy'))
    female_given_name_dict.update(dict.fromkeys([69, 70, 71, 72], 'Lydia'))
    female_given_name_dict.update(dict.fromkeys([73, 74, 75, 76], 'Margaret'))
    female_given_name_dict.update(dict.fromkeys([77, 78, 79, 80], 'Mary'))
    female_given_name_dict.update(dict.fromkeys([81, 82, 83, 84], 'Molly'))
    female_given_name_dict.update(dict.fromkeys([85, 86, 87, 88], 'Nora'))
    female_given_name_dict.update(dict.fromkeys([89, 90, 91, 92], 'Rosie'))
    female_given_name_dict.update(dict.fromkeys([93, 94, 95, 96], 'Sarah'))
    female_given_name_dict.update(dict.fromkeys([97, 98, 99, 100], 'Victoria'))

    surname_dict = dict.fromkeys([1, 2, 3, 4], 'Barker')
    surname_dict.update(dict.fromkeys([5, 6, 7, 8], 'Brown'))
    surname_dict.update(dict.fromkeys([9, 10, 11, 12], 'Butler'))
    surname_dict.update(dict.fromkeys([13, 14, 15, 16], 'Carter'))
    surname_dict.update(dict.fromkeys([17, 18, 19, 20], 'Chapman'))
    surname_dict.update(dict.fromkeys([21, 22, 23, 24], 'Collins'))
    surname_dict.update(dict.fromkeys([25, 26, 27, 28], 'Cook'))
    surname_dict.update(dict.fromkeys([29, 30, 31, 32], 'Davies'))
    surname_dict.update(dict.fromkeys([33, 34, 35, 36], 'Gray'))
    surname_dict.update(dict.fromkeys([37, 38, 39, 40], 'Green'))
    surname_dict.update(dict.fromkeys([41, 42, 43, 44], 'Harris'))
    surname_dict.update(dict.fromkeys([45, 46, 47, 48], 'Jackson'))
    surname_dict.update(dict.fromkeys([49, 50, 51, 52], 'Jones'))
    surname_dict.update(dict.fromkeys([53, 54, 55, 56], 'Lloyd'))
    surname_dict.update(dict.fromkeys([57, 58, 59, 60], 'Miller'))
    surname_dict.update(dict.fromkeys([61, 62, 63, 64], 'Roberts'))
    surname_dict.update(dict.fromkeys([65, 66, 67, 68], 'Smith'))
    surname_dict.update(dict.fromkeys([69, 70, 71, 72], 'Taylor'))
    surname_dict.update(dict.fromkeys([73, 74, 75, 76], 'Thomas'))
    surname_dict.update(dict.fromkeys([77, 78, 79, 80], 'Turner'))
    surname_dict.update(dict.fromkeys([81, 82, 83, 84], 'Watson'))
    surname_dict.update(dict.fromkeys([85, 86, 87, 88], 'White'))
    surname_dict.update(dict.fromkeys([89, 90, 91, 92], 'Williams'))
    surname_dict.update(dict.fromkeys([93, 94, 95, 96], 'Wood'))
    surname_dict.update(dict.fromkeys([97, 98, 99, 100], 'Young'))

    given_name_roll = get_random_number(100)
    surname_roll = get_random_number(100)

    given_name = male_given_name_dict.get(given_name_roll) if (gender == 'Male') \
        else female_given_name_dict.get(given_name_roll)
    surname = surname_dict.get(surname_roll)

    return given_name + ' ' + surname
